FROM node:4.2.3
EXPOSE 1337
RUN mkdir /qapp
WORKDIR /qapp
COPY api ./api/
COPY assets /qapp/assets/
COPY config /qapp/config/
RUN mkdir /qapp/node_modules/
COPY tasks /qapp/tasks/
COPY views /qapp/views/
COPY README.md .
COPY app.js .
COPY package.json .
COPY process.json .
COPY schema_dump.sql .
RUN mkdir /qapp/.tmp
WORKDIR /qapp
RUN apt-get update && \
		apt-get install -y postgresql-client-9.4
RUN npm install --production
RUN ln -s /qapp/node_modules/pm2/bin/pm2 /sbin/pm2
CMD ["pm2","start","--no-daemon","process.json"]