/**
 * AccountController
 *
 * @description :: Server-side logic for managing accounts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var accntUtils = require("../services/utils/accntutils.js");
var pgp = require('pg-promise')();
var cache = require('memory-cache');
var spawn = require('child-process-promise').spawn;
var exec = require('child-process-promise').exec;
var fsp = require('fs-promise');
var errorCode = require("../resources/ErrorCodes.js");
var TEMPDIR = process.env.HOME + '/';
var baseBrokerPort = 1500;
var Promise = require("bluebird");
var AWS = require('aws-sdk');
AWS.config.setPromisesDependency(require('bluebird'));
var s3 = new AWS.S3({ apiVersion: sails.config.awscfg.s3apiversion, region: sails.config.awscfg.s3region, accessKeyId: sails.config.awscfg.accessKeyId, secretAccessKey: sails.config.awscfg.secretAccessKey });
var sg = require('sendgrid')(sails.config.sendgrid.apikey);
var NginxConfFile = require('nginx-conf').NginxConfFile;
Promise.promisifyAll(NginxConfFile);
var request = require('request-promise');
var sleep = require('sleep-promise');
var crypto = require("crypto");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var promisedCipherService = Promise.promisifyAll(require("../services/utils/CipherService.js"));
var request = require('request-promise'); 


if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
{
  var client = redis.createClient(sails.config.connections.localTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

var connection = 
{
  user: 'postgres', 
  database: 'postgres', 
  host: '127.0.0.1',
  password: 'postgres',
  port: 5432
};

if(sails.config.environment === 'production')
{
  connection.host = sails.config.connections.awsrds.host;
  connection.password = sails.config.connections.awsrds.password;
}

module.exports = 
{
	add: function(req, res)
	{
		if(!req.body.priceplan || !req.body.fullname || !req.body.username || !req.body.accountname || !req.body.countrycode ||
     !req.body.phonenumber || !req.body.address || !req.body.custadmin || req.body.acctmgr || !req.body.token || !req.body.password)
		{
			var error = new Error();
			error.code = 400051;
			error.message = errorCode['400051'];
      sails.log.error(errorCode['400051']);
			return res.negotiate(error);
		}
    sails.log.info("Setting the timeout to 10 minutes");
    req.connection.setTimeout(10*60*1000);
    var startTime = Date.now();
		var accountEntry = {};
		var db = {};
		var response = {};
    var dockerStarted = false;
    var createdWorker = false;
    var workerNum;
    var DOCKERIMG;
    client.getAsync('invitation:' + req.body.custadmin)
    .then(function (token)
    {
      if(token !== req.body.token)
      {
        var error = new Error();
        error.code = 401501;
        error.message = errorCode['401501'];
        throw error;
      }
      else
      {
        accountEntry.accountid = accntUtils.randomgen(8);
        var pDelToken = client.delAsync('invitation:' + req.body.custadmin);
        var pFindAccount = Account.findOne({ accountid: accountEntry.accountid });
        return Promise.all([pFindAccount, pDelToken]);
      }
    })		
    .then(function (result)
    {
    	if(result[0])
    	{
    		var error = new Error();
    		error.code = 400052;
    		error.message = errorCode['400052'];
        sails.log.error(errorCode['400052']);
    		throw error;
    	}
    	else
    	{
        connection.user = 'postgres';
        connection.database = 'postgres';
        sails.config.environment === 'production' ? (connection.password = sails.config.connections.awsrds.password) : (connection.password = 'postgres');
    		db = cache.get(connection.database);
      	if(!db)
      	{
      	  db = pgp(connection);
      	  cache.put(connection.database, db);
      	}
    		var query = 'CREATE DATABASE "' + accountEntry.accountid + '"';
        sails.log.info("Creating the database for account", accountEntry.accountid);
    		return db.any(query);
    	}
    })
    .then(function (result)
    {
    	accountEntry.database = accountEntry.accountid;
      var query = 'CREATE ROLE "' + accountEntry.database + '"';
      sails.log.info("Creating the role");
      return db.any(query);
    })
    .then(function (result)
    {
      var query1 = 'ALTER ROLE "' +  accountEntry.database + '" WITH PASSWORD \'' + accountEntry.database + '\';';
      var query2 = 'ALTER ROLE "' +  accountEntry.database + '" LOGIN;';
      var query3 = 'REVOKE CONNECT ON DATABASE "' + accountEntry.database + '" FROM PUBLIC';
      var query4 = 'GRANT CONNECT ON DATABASE "' + accountEntry.database + '" TO "' + accountEntry.database + '"';
      sails.log.info("Setting the privileges");
      var pDbTx = db.tx(function (transaction)
      {
        var task1 = transaction.none(query1);
        var task2 = transaction.none(query2);
        var task3 = transaction.none(query3);
        var task4 = transaction.none(query4);
        return transaction.batch([task1, task2, task3, task4]);          
      });
      var sqlDumpFileParams =
      {
        Bucket: sails.config.awscfg.configbucket,
        Key: 'schema_dump.sql'
      };      
      var pGetSqlDump = s3.getObject(sqlDumpFileParams).promise();
      var pemParams = 
      {
        Bucket: sails.config.awscfg.configbucket,
        Key: 'swarm-key1'
      };
      var pGetPem = s3.getObject(pemParams).promise();
      var pubParams =
      {
        Bucket: sails.config.awscfg.configbucket,
        Key: 'swarm-key1.pub'
      };      
      var pGetPub = s3.getObject(pubParams).promise();
      var dockerEnvFileParams = 
      {
        Bucket: sails.config.awscfg.configbucket,
        Key: 'dockerenv.lst'
      };
      var pGetEnvFile = s3.getObject(dockerEnvFileParams).promise();
      var dockerImgFileParams = 
      {
        Bucket: sails.config.awscfg.configbucket,
        Key: 'dockerimg'
      };
      var pGetDockerImg = s3.getObject(dockerImgFileParams).promise();
      sails.log.info("Downloading the env file"); 
      return Promise.all([pDbTx, pGetSqlDump, pGetPem, pGetPub, pGetEnvFile, pGetDockerImg]);
    })
    .then(function (result)
    {
      var sqlDumpFileCreate  = fsp.writeFile(TEMPDIR + 'schema_dump.sql', result[ 1 ].Body.toString('utf-8'));
      sails.log.info("Creating the postgres credentials file");
			var pPgpasFileCreate = fsp.writeFile(TEMPDIR + '.pgpass', connection.host + ':' + connection.port + ':' + accountEntry.database + ':' + accountEntry.database + ':' + accountEntry.database);
      var pemFileCreate  = fsp.writeFile(TEMPDIR + 'swarm-key1', result[ 2 ].Body.toString('utf-8'));
      var pubFileCreate  = fsp.writeFile(TEMPDIR + 'swarm-key1.pub', result[ 3 ].Body.toString('utf-8'));
      var pDockerEnvFileCreate = fsp.writeFile(TEMPDIR + 'dockerenv.lst', result[ 4 ].Body.toString('utf-8'));
      DOCKERIMG = result[ 5 ].Body.toString('utf-8');
      sails.log.info("Using Docker Image", DOCKERIMG);
      return Promise.all([pPgpasFileCreate, sqlDumpFileCreate, pemFileCreate, pubFileCreate, pDockerEnvFileCreate]);
    })
    .then(function (result)
    {
    	var pgPassModChange = exec('chmod 0600 ' + TEMPDIR + '.pgpass');
      var pemModChange = exec('chmod 0400 ' + TEMPDIR + 'swarm-key1');
      var pubFileChange = exec('chmod 0400 ' + TEMPDIR + 'swarm-key1.pub');
      return Promise.all([pgPassModChange, pemModChange, pubFileChange]);
    })
    .then(function (result)
    {
      sails.log.info("Creating the tables and encrypting the supplied password");
			var pPsql = spawn('psql', ['-h', connection.host, '-d', accountEntry.database, '-U', accountEntry.database, '-f', TEMPDIR + 'schema_dump.sql', '-w']);
      var pHashPass = promisedCipherService.hashPasswordAsync(req.body.password);
			return Promise.all([pPsql, pHashPass]);
    })
    .then(function (result)
    {
    	connection.database = accountEntry.database;
    	db = cache.get(connection.database);
      if(!db)
      {
        connection.user = accountEntry.database;
        connection.password = accountEntry.database;
        db = pgp(connection);
        cache.put(connection.database, db);
      }
    	var query1 = "UPDATE COMPANIES SET accountid='" + connection.database + "', name='" + req.body.accountname + "\'";
      var query2 = "UPDATE USERS SET fullname='" + req.body.fullname + "', username='" + req.body.username + "', password='" + result[1] + "', emailid='" + req.body.custadmin + "', countrycode='" + req.body.countrycode + "', phonenumber='" + req.body.phonenumber + "', accountid='" + accountEntry.database + "'";
    	
    	sails.log.info("Setting up the user with supplied password and company");
      return db.tx(function (transaction)
    	{
    		var task1 = transaction.none(query1);
    		var task2 = transaction.none(query2);
    		return transaction.batch([task1, task2]);
    	});
    })
    .then(function (result)
    {
      return exec('/usr/bin/sudo /usr/local/bin/aws ecr get-login --region ap-southeast-1');
    })
    .then(function (result)
    {
      var ecrLoginCommand = "/usr/bin/sudo /usr/bin/" + result.stdout.replace('-e none ','');
      var pEcrLogin = exec(ecrLoginCommand);
      var pNodeList = exec("/usr/bin/sudo /usr/bin/docker node ls --filter name='swarm-worker' | grep 'Ready' | grep -v 'AVAILABILITY' | wc -l");
      var pServiceList = exec("/usr/bin/sudo /usr/bin/docker service ls | grep -v 'IMAGE' | wc -l");
      return Promise.all([pEcrLogin, pNodeList, pServiceList]);
    })
    .then(function (result)
    {
      sails.log.info("Current Number of Worker Nodes", result[ 1 ].stdout.replace(/(\r\n|\n|\r)/gm,""));
      sails.log.info("Current Number of Services", result[ 2 ].stdout.replace(/(\r\n|\n|\r)/gm,""));
      if(result[ 1 ].stdout * 3 <= result[ 2 ].stdout)
      {
        workerNum = parseInt(result[ 1 ].stdout) + 1;
        sails.log.info("Starting an EC2 instance for Worker Number: ", workerNum);
        createdWorker = !createdWorker;
        var pStartWorker = spawn('/usr/bin/sudo', ['/usr/local/bin/docker-machine', 'create', '--driver=amazonec2', '--amazonec2-keypair-name=swarm-key1', '--amazonec2-ssh-keypath=/home/ubuntu/swarm-key1','--amazonec2-instance-type=t2.micro', '--amazonec2-region=ap-south-1', '--amazonec2-use-private-address=true', '--amazonec2-root-size=8', 'swarm-worker' + workerNum]);
        var pGetJoinToken = exec('/usr/bin/sudo /usr/bin/docker swarm join-token -q worker');
        var pGetManagerIP = exec('/sbin/ifconfig eth0 | grep \'inet addr\' | cut -d: -f2 | awk \'{print $1}\'');
        return Promise.all([pStartWorker, pGetJoinToken, pGetManagerIP]);
      }
      else
      {
        return result;
      }
    })
    .then(function (result)
    {
      if(createdWorker)
      {
        sails.log.info("New worker joining the swarm");
        var joinCmd = '/usr/bin/sudo /usr/bin/docker swarm join --token=' + result[ 1 ].stdout + ' ' + result[ 2 ].stdout + ':2377';
        joinCmd = joinCmd.replace(/(\r\n|\n|\r)/gm,"");
        return spawn('/usr/bin/sudo', ['/usr/local/bin/docker-machine', 'ssh', 'swarm-worker' + workerNum, joinCmd]);
      }
      else
      {
        return result;
      }
    })
    .then(function (result)
    {
    	var newBucketParams = 
    	{
			  Bucket: accountEntry.database,
			  ACL: 'private',
			  CreateBucketConfiguration: {
			    LocationConstraint: 'ap-south-1'
			  }
			};
      sails.log.info("Creating the S3 bucket");   
			var pBucketCreate = s3.createBucket(newBucketParams).promise();
      return pBucketCreate;
    })
    .then(function (result)
    {
    	accountEntry.priceplan = req.body.priceplan;
    	accountEntry.name = req.body.accountname;
    	accountEntry.address = req.body.address;
    	accountEntry.url = 'https://app.queueload.com/' + accountEntry.accountid + '/';
    	accountEntry.s3bucket = accountEntry.database;
    	accountEntry.custadmin = req.body.custadmin;
      accountEntry.config = {};
    	var pAccountCreate = Account.create(accountEntry);
      return pAccountCreate;
    })
    .then(function (result)
    {
      response = result;
      var port = baseBrokerPort + response.id;
      sails.log.info("Starting the service");
      var pAppStart = exec('/usr/bin/sudo /usr/bin/docker service create --with-registry-auth --name=broker-' + response.accountid + ' --env ACCOUNTID=' + response.accountid + ' $(cat dockerenv.lst | xargs printf "--env %s ") --replicas=1 --constraint="node.role!=manager" --restart-condition=on-failure --restart-delay=15s --restart-max-attempts=5 --publish=' + port + ':1337 ' + DOCKERIMG);
      return pAppStart;
    })
    .then(function (result)
    {      
      dockerStarted = !dockerStarted;
      sails.log.info("Cleaning up downloaded files...");
      var pRemovePgpass = fsp.remove(TEMPDIR + '.pgpass');
      var pRemoveDockerEnvFile = fsp.remove(TEMPDIR + 'dockerenv.lst');
      var pRemovePemFile = exec('/usr/bin/sudo rm -f ' + TEMPDIR + 'swarm-key1');
      var pRemovePubFile = exec('/usr/bin/sudo rm -f ' + TEMPDIR + 'swarm-key1.pub');
      var pSchemaDumpFile = fsp.remove(TEMPDIR + 'schema_dump.sql');
      return Promise.all([pRemovePgpass, pRemoveDockerEnvFile, pRemovePemFile, pRemovePubFile, pSchemaDumpFile]);
    })
    .then(function ()
    {
      return NginxConfFile.createAsync('/etc/nginx/nginx.conf');
    })
    .then(function (conf)
    {
      conf.die('/etc/nginx/nginx.conf');

      conf.live('/tmp/nginx.conf.test');

      var locnum = conf.nginx.http.server[0].location.length;
      var port = baseBrokerPort + response.id;
      conf.nginx.http.server[0]._add('location', '/' + response.accountid + '/');
      conf.nginx.http.server[0].location[locnum]._add('proxy_pass', 'http://127.0.0.1:' + port + '/');
      conf.nginx.http.server[0].location[locnum]._add('proxy_http_version', '1.1');
      conf.nginx.http.server[0].location[locnum]._add('proxy_set_header', 'Upgrade $http_upgrade');
      conf.nginx.http.server[0].location[locnum]._add('proxy_set_header', 'Connection "upgrade"');
      conf.nginx.http.server[0].location[locnum]._add('proxy_set_header', 'Connection        ""');
      conf.nginx.http.server[0].location[locnum]._add('proxy_set_header', 'Host              $host');
      conf.nginx.http.server[0].location[locnum]._add('proxy_set_header', 'X-Forwarded-For   $proxy_add_x_forwarded_for');
      conf.nginx.http.server[0].location[locnum]._add('proxy_set_header', 'X-Real-IP         $remote_addr');

      var flushAsync = function ()
      {
        return new Promise(function(resolve,reject)
        {
          conf.flush(function(err,flushed)
          {
            if(err !== null)
            {
              return reject(err);
            }
            else
            {
              sails.log.info("New nginx configuration dumped to temporary file");
              resolve(flushed);               
            }            
          });
        });
      };
      return flushAsync();
    })
    .then(function (result)
    {
      sails.log.info("Testing the new nginx configuration");
      return exec('sudo /usr/sbin/nginx -t -c /tmp/nginx.conf.test');
    })
    .then(function (result)
    {
      var error = result.stderr.toString();
      var output = result.stdout.toString();
      var text = error.concat(output);
      if(text.search("test is successful"))
      {
        sails.log.info("Test is successful");
        sails.log.info("Backing up the current nginx configuration");
        return exec('sudo /bin/cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.' + response.accountid);
      }
    })
    .then(function (result)
    {
      sails.log.info("Applying the new nginx configuration");
      return exec('sudo /bin/cp /tmp/nginx.conf.test /etc/nginx/nginx.conf'); 
    })
    .then(function (result)
    {       
      sails.log.info("Reloading nginx service");
      return exec('sudo /usr/sbin/service nginx reload');
    })
    .then(function (result)
    {
      if(createdWorker)
      {
        sails.log.info("Giving 5 minutes for the new worker to setup and initiate the new broker...");
        return sleep(300000);        
      }
      else
      {
        sails.log.info("Giving 60 seconds for the new broker to come up...");
        return sleep(60000);
      }
    })
    .then(function (result)
    {
      sails.log.info("Testing the broker now...");
      var options = 
      {
        method: 'POST',
        uri: 'https://broker.queueload.com/' + response.accountid + '/auth/signin',
        body: {
            emailid : req.body.custadmin,
            password: req.body.password
        },
        resolveWithFullResponse: true,
        json: true
      };
      return request(options);
    })
    .then(function (result)
    {
      if(result.statusCode !== 200)
      {
        var error = new Error();
        error.code = 400053;
        error.message = errorCode['400053'];
        sails.log.error(errorCode['400053']);
        throw error;
      }
      else
      {
        sails.log.info("Test is successful");
        var custEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: req.body.custadmin
                  }
                ],
                'substitutions': {
                  '-name-': req.body.fullname,
                  '-accountid-': response.accountid,
                  '-accountadmin-': req.body.custadmin
                },
                subject: 'Your QueueLoad account is ready'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': '309c9f95-a5c3-498d-bfb2-7f49d9a7cdf1',
          }
        });
        sails.log.info("Sending the welcome email to customer");
        var endTime = Date.now();
        var qlOpsEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: 'founders@queueload.com'
                  }
                ],
                'substitutions': {
                  '-accountid-': response.accountid,
                  '-accountadmin-': req.body.custadmin,
                  '-time-' : new Date().toUTCString()
                },
                subject: 'New account sign-up notification'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': 'dee3cff9-a361-408e-a6ee-d7e9328da12e',
          }
        });
        sails.log.info("Total time taken for client provisioning", endTime - startTime);
        var pCalls = [sg.API(custEmail), sg.API(qlOpsEmail)];
        return Promise.all(pCalls);
      }
    })
    .then(function () 
    {
      return res.ok({"Account created" : response});
      },
      function(error) {
        sails.log.error("Something went wrong");
        sails.log.error("Error details 1", error.toString());
        var timeInMs = Date.now();
        var email = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
          personalizations: [
            {
              to: [
                {
                  email: 'smruti.mandal@queueload.com'
                }
              ],
              subject: '[CRITICAL ALERT] Error during Account Setup!!'
            }
          ],
          from: {
            email: 'noreply@queueload.com'
          },
          content: [
            {
              type: 'text/plain',
              value: "There was a problem in setting up account for the following customer: \n\n \
                    Time:\t\t\t\t\t\t\t" + timeInMs + "\n \
                    Priceplan:\t\t\t\t\t\t" + req.body.priceplan + "\n \
                    Full Name:\t\t\t\t\t\t" + req.body.fullname + "\n \
                    Username:\t\t\t\t\t\t" + req.body.username + "\n \
                    Account Name:\t\t\t\t\t\t" + req.body.accountname + "\n \
                    Country Code:\t\t\t\t\t\t" + req.body.countrycode + "\n \
                    Phone Number:\t\t\t\t\t\t" + req.body.phonenumber + "\n \
                    Address:\t\t\t\t\t\t" + req.body.address + "\n \
                    Employee ID:\t\t\t\t\t\t" + req.body.employeeid + "\n \
                    Customer Admin:\t\t\t\t\t\t" + req.body.custadmin + "\n \
                    Assigned Account ID:\t\t\t\t\t" + response.accountid + "\n\n \
                    [Error]:\n\n" + error + "\n\n \
              Please contact the customer immediately!!"
            }
          ]
        }
      });
      sails.log.error("Failed to setup account for the following request");
      var pCleanup = [sg.API(email), res.negotiate(error)];
      if(dockerStarted)
      {
        sails.log.info("Stopping the service");
        var pAppStop = spawn ('/usr/bin/sudo', ['/usr/bin/docker', 'service', 'rm', 'broker-' + response.accountid]);
        pCleanup.push(pAppStop);
      }
      pCleanup.push(fsp.remove(TEMPDIR + '.pgpass'));
      pCleanup.push(fsp.remove(TEMPDIR + 'dockerenv.lst'));
      pCleanup.push(exec('/usr/bin/sudo rm -f ' + TEMPDIR + 'swarm-key1'));
      pCleanup.push(exec('/usr/bin/sudo rm -f ' + TEMPDIR + 'swarm-key1.pub'));
      pCleanup.push(fsp.remove(TEMPDIR + 'schema_dump.sql'));
      return Promise.all(pCleanup);
      }
    )
    .catch(function (error)
    {
      sails.log.error("Something went wrong");
      sails.log.error("Error details 2", error.toString());
      var timeInMs = Date.now();
      var email = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
          personalizations: [
            {
              to: [
                {
                  email: 'smruti.mandal@queueload.com'
                }
              ],
              subject: '[CRITICAL ALERT] Error during Account Setup!!'
            }
          ],
          from: {
            email: 'noreply@queueload.com'
          },
          content: [
            {
              type: 'text/plain',
              value: "There was a problem in setting up account for the following customer: \n\n \
                    Time:\t\t\t\t\t\t\t" + timeInMs + "\n \
                    Priceplan:\t\t\t\t\t\t" + req.body.priceplan + "\n \
                    Full Name:\t\t\t\t\t\t" + req.body.fullname + "\n \
                    Username:\t\t\t\t\t\t" + req.body.username + "\n \
                    Account Name:\t\t\t\t\t\t" + req.body.accountname + "\n \
                    Country Code:\t\t\t\t\t\t" + req.body.countrycode + "\n \
                    Phone Number:\t\t\t\t\t\t" + req.body.phonenumber + "\n \
                    Address:\t\t\t\t\t\t" + req.body.address + "\n \
                    Employee ID:\t\t\t\t\t\t" + req.body.employeeid + "\n \
                    Customer Admin:\t\t\t\t\t\t" + req.body.custadmin + "\n \
                    Assigned Account ID:\t\t\t\t\t" + response.accountid + "\n\n \
                    [Error]:\n\n" + error + "\n\n \
              Please contact the customer immediately!!"
            }
          ]
        }
      });
      sails.log.error("Failed to setup account for the following request");
      var pCleanup = [sg.API(email), res.negotiate(error)];
      if(dockerStarted)
      {
        sails.log.info("Stopping the service");
        var pAppStop = spawn ('/usr/bin/sudo', ['/usr/bin/docker', 'service', 'rm', 'broker-' + response.accountid]);
        pCleanup.push(pAppStop);
      }
      pCleanup.push(fsp.remove(TEMPDIR + '.pgpass'));
      pCleanup.push(fsp.remove(TEMPDIR + 'dockerenv.lst'));
      pCleanup.push(exec('/usr/bin/sudo rm -f ' + TEMPDIR + 'swarm-key1'));
      pCleanup.push(exec('/usr/bin/sudo rm -f ' + TEMPDIR + 'swarm-key1.pub'));
      pCleanup.push(fsp.remove(TEMPDIR + 'schema_dump.sql'));
      return Promise.all(pCleanup);
    });
	},
  invite: function(req, res)
  {
    if(!req.body.emailid || !req.body.captcharesponse)
    {
      var error  = new Error();
      error.code = 400051;
      error.message = errorCode['400051'];
      return res.negotiate(error);
    }
    req.body.emailid = req.body.emailid.trim();
    var hash = crypto.createHash(sails.config.authcfg.inviteTknAlgorithm);
    hash.update(crypto.randomBytes(sails.config.authcfg.inviteTknLen).toString("hex") + process.hrtime().toString());
    var token = hash.digest("hex");
    var pServiceList = exec("/usr/bin/sudo /usr/bin/docker service ls | grep -v 'IMAGE' | wc -l");
    var pPendingInviteList = client.scanAsync(0, "MATCH", "*invitation*", "COUNT", 20000);
    var pInvite = client.multi()
      .set('invitation:' + req.body.emailid, token)
      .EXPIRE('invitation:' + req.body.emailid, sails.config.authcfg.inviteTknExpiresIn)
      .execAsync();
    var options = 
    {
      method: 'POST',
      uri: 'https://www.google.com/recaptcha/api/siteverify',
      form: 
      {
        secret: sails.config.captchacfg.secret,
        response: req.body.captcharesponse,

      },
      headers:
      {
        'content-type': 'application/x-www-form-urlencoded'
      }
    };
    request(options)
    .then(function (body)
    {
      if(!JSON.parse(body).success)
      {
        var error = new Error();
        error.code = 400055;
        error.message = errorCode['400055'];
        throw error;
      }
      else
      {
        return Promise.all([pInvite, pPendingInviteList, pServiceList]);  
      }
    })
    .then(function(result)
    {
      console.log("Pending invite list", result[1][1].length);
      console.log("Number of running services", result[2].stdout.replace(/(\r\n|\n|\r)/gm,""));
      console.log("Maximum number of accounts", process.env.MAXACCOUNTS);
      if(parseInt(result[1][1].length) + parseInt(result[2].stdout.replace(/(\r\n|\n|\r)/gm,"")) >= process.env.MAXACCOUNTS)
      {
        var error = new Error();
        error.code = 400054;
        error.message = errorCode['400054'];
        throw error;
      }
      else
      {
        var custEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: req.body.emailid
                  }
                ],
                'substitutions': {
                  '-token-': token,
                  '-emailid-': req.body.emailid
                },
                subject: 'Invitation from QueueLoad'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': '5d3ce4f8-b9fa-450e-b1c0-b0355bd51321',
          }
        });
        var qlEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: 'founders@queueload.com'
                  }
                ],
                'substitutions': {
                  '-emailid-': req.body.emailid
                },
                subject: 'New invitation notification'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': '8e915b7c-87c2-48ca-adfb-6919d0377aaf',
          }
        });
        sails.log.info("Sending invite email notifications");
        return Promise.all([sg.API(custEmail), sg.API(qlEmail)]);
      }
    })
    .then(function (result)
    {
      sails.log.info("Invitation sent successfully");
      return res.ok();
    })
    .catch(function (error)
    {
      if(error.code === 400054)
      {
        var custEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: req.body.emailid
                  }
                ],
                subject: 'We are full at the moment'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': 'c0b70c18-1d6c-4105-8238-ef515d5dd42b',
          }
        });
        var qlEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: 'founders@queueload.com'
                  }
                ],
                'substitutions': {
                  '-emailid-': req.body.emailid
                },
                subject: '[Alert] Customer couldn\'t sign-up due to no free slots'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': 'f10ba79a-14f6-4c90-92a9-0e674b8edfd5',
          }
        });
        return Promise.all([sg.API(custEmail), sg.API(qlEmail)], res.negotiate(error));
      }
      else
      {
        return res.negotiate(error);
      }
    });
  },
  tokenchk: function(req, res)
  {
    if(!req.body.custadmin || !req.body.token)
    {
      var error  = new Error();
      error.code = 400051;
      error.message = errorCode['400051'];
      return res.negotiate(error);      
    }
    client.getAsync('invitation:' + req.body.custadmin)
    .then(function (token)
    {
      if(token !== req.body.token)
      {
        var error = new Error();
        error.code = 401501;
        error.message = errorCode['401501'];
        throw error;
      }
      else
      {
        return res.ok();
      }
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  },
  invitecheck: function(req, res)
  {
    var pServiceList = exec("/usr/bin/sudo /usr/bin/docker service ls | grep -v 'IMAGE' | wc -l");
    var pPendingInviteList = client.scanAsync(0, "MATCH", "*invitation*", "COUNT", 20000);
    Promise.all([pServiceList, pPendingInviteList])
    .then(function (result)
    {
      console.log("Number of running services", result[0].stdout.replace(/(\r\n|\n|\r)/gm,""));
      console.log("Number of pending invites", result[1][1].length);
      console.log("Maximum number of accounts", process.env.MAXACCOUNTS);
      if(parseInt(result[0].stdout.replace(/(\r\n|\n|\r)/gm,"")) + result[1][1].length >= process.env.MAXACCOUNTS)
      {
        var error = new Error();
        error.code = 400054;
        error.message = errorCode['400054'];
        throw error;
      }
      else
      {
        return res.ok();
      }
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  }
};