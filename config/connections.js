/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.connections.html
 */

module.exports.connections = {

  /***************************************************************************
  *                                                                          *
  * Local disk storage for DEVELOPMENT ONLY                                  *
  *                                                                          *
  * Installed by default.                                                    *
  *                                                                          *
  ***************************************************************************/
  localDiskDb: {
    adapter: 'sails-disk'
  },
  localAccountsRepo: {
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'accountsadmin',
    password: 'accountsadmin',
    database: 'accountsdb' 
  },
  localTknStore: {
    host: 'localhost',
    port: 6379
  },
  awsAccountsRepo: {
    adapter: 'sails-postgresql',
    host: 'qlrds1.ckhrxytuacwp.ap-south-1.rds.amazonaws.com',
    user: 'accountsadmin',
    password: 'd4ac7a19c400976e72dc1865ef892244',
    database: 'accountsdb' 
  },
  awsrds:
  {
    adapter: 'sails-postgresql',
    host: 'qlrds1.ckhrxytuacwp.ap-south-1.rds.amazonaws.com',
    user: 'postgres',
    password: '511cf90b86a4fbf7ce2dd40298581b12',
    database: 'postgres' 
  },
  awsTknStore: {
    host: 'qlcache.rbfvtg.0001.aps1.cache.amazonaws.com',
    port: 6379
  }
};
